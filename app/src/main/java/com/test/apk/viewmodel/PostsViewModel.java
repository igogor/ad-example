package com.test.apk.viewmodel;

import com.test.apk.dto.Posts;
import com.test.apk.repository.PostsRepository;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class PostsViewModel extends ViewModel {
    private MutableLiveData<Posts> posts;
    private PostsRepository repository= new PostsRepository();

    public MutableLiveData<Posts> getPosts() {
        if (posts == null) {
            posts = new MutableLiveData<>();
        }
        posts = repository.getPosts();
        return posts;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        posts = null;
    }
}
