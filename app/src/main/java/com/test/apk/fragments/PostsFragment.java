package com.test.apk.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.test.apk.R;
import com.test.apk.adapter.PostAdapter;
import com.test.apk.dto.Post;
import com.test.apk.helpers.NetworkHelper;
import com.test.apk.viewmodel.PostsViewModel;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class PostsFragment extends Fragment {
    private PostAdapter adapter;
    private PostsViewModel postsViewModel;
    private CallbackListener listener;
    @BindView(R.id.list)
    RecyclerView recyclerView;
    @BindView(R.id.messageText)
    TextView infoText;
    @BindView(R.id.refreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.adView)
    AdView adView;
    private Unbinder unbinder;

    public interface CallbackListener {
        void onPostClick(Post post);
        void onButtonClick(Post post);
    }

    public PostsFragment() {
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof CallbackListener) {
            listener = (CallbackListener) context;
        } else {
            throw new ClassCastException(context.toString() + " must implement CallbackListener");
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_main, container, false);

        postsViewModel = ViewModelProviders.of(this).get(PostsViewModel.class);
        unbinder = ButterKnife.bind(this, view);
        updateUI();

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        swipeRefreshLayout.setOnRefreshListener(() -> {
            if (NetworkHelper.isOnline(getContext())) {
                updateUI();
                loadData();
            } else {
                cancelRefresh();
            }
        });
        return view;
    }

    private void updateUI() {
        if (NetworkHelper.isOnline(getContext())) {
            infoText.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
            if (adapter == null) {
                adapter = new PostAdapter(this.getActivity(), listener);
                recyclerView.setAdapter(adapter);
            } else {
                adapter.notifyDataSetChanged();
            }
            recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            recyclerView.setHasFixedSize(true);
        } else {
            infoText.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            infoText.setText(R.string.info_no_connection);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (NetworkHelper.isOnline(getContext())) {
            loadData();
        }
    }

    @Override
    public void onDestroy() {
        listener = null;
        unbinder.unbind();
        super.onDestroy();
    }

    private void loadData() {
        postsViewModel.getPosts().observe(this, posts -> {
            adapter.submitList(posts.getPosts());
            getActivity().setTitle(posts.getHead());
            cancelRefresh();
        });
    }

    private void cancelRefresh() {
        swipeRefreshLayout.setRefreshing(false);
    }
}
