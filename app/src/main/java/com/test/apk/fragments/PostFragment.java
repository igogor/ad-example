package com.test.apk.fragments;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.squareup.picasso.Picasso;
import com.test.apk.R;
import com.test.apk.WebViewActivity;
import com.test.apk.dto.Post;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class PostFragment extends Fragment {
    private static final String DATA = "post_data";
    @BindView(R.id.post_image)
    ImageView image;
    @BindView(R.id.post_title)
    TextView title;
    @BindView(R.id.post_text)
    TextView text;
    @BindView(R.id.post_button)
    Button button;
    private Unbinder unbinder;
    @BindView(R.id.adView)
    AdView adView;
    private Post post;

    public static PostFragment newInstance(Post post) {

        Bundle args = new Bundle();
        args.putParcelable(DATA, post);
        PostFragment fragment = new PostFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            post = getArguments().getParcelable(DATA);
        }
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.activity_post, container, false);
        unbinder = ButterKnife.bind(this, view);

        if (post.getImgLink() == null) {
            image.setImageResource(R.drawable.no_image_icon);
        } else {
            Picasso.get().load(post.getImgLink()).into(image);
        }
        title.setText(post.getName());
        text.setText(post.getDescr());
        button.setText(post.getBtnText());
        button.setOnClickListener(v -> {
            if (post.isWebView()) {
                startActivity(WebViewActivity.newIntent(getActivity(), post.getSiteLink(), getActivity().getTitle().toString()));
            } else {
                startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(post.getSiteLink())));
        }});

        AdRequest adRequest = new AdRequest.Builder().build();
        adView.loadAd(adRequest);

        return view;
    }

    @Override
    public void onDestroy() {
        unbinder.unbind();
        super.onDestroy();
    }
}
