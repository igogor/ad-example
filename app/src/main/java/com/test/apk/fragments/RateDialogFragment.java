package com.test.apk.fragments;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.TextView;

import com.test.apk.R;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.DialogFragment;
import butterknife.BindView;
import butterknife.ButterKnife;

public class RateDialogFragment extends DialogFragment {
    @BindView(R.id.ratingScale)
    TextView ratingText;
    @BindView(R.id.feedback)
    EditText feedback;
    @BindView(R.id.ratingBar)
    RatingBar ratingBar;
    private AlertDialog alertDialog;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        View view = LayoutInflater.from(getActivity()).inflate(R.layout.dialog_rate, null);
        ButterKnife.bind(this, view);

        ratingBar.setOnRatingBarChangeListener((ratingBar, rating, fromUser) -> {
            ratingText.setText(String.valueOf(rating));
            switch ((int) ratingBar.getRating()) {
                case 1:
                    ratingText.setText(R.string.rating_scale_vbad);
                    break;
                case 2:
                    ratingText.setText(R.string.rating_scale_bad);
                    break;
                case 3:
                    ratingText.setText(R.string.rating_scale_good);
                    break;
                case 4:
                    ratingText.setText(R.string.rating_scale_great);
                    break;
                case 5:
                    ratingText.setText(R.string.rating_scale_awesome);
                    break;
                default:
                    ratingText.setText("");
            }
            validateView();
        });

        feedback.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s.length() != 0) {
                    enableSendButton(true);
                } else {
                    enableSendButton(false);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        alertDialog = new AlertDialog.Builder(getActivity())
                .setView(view)
                .setTitle("Rate Us")
                .setCancelable(false)
                .setPositiveButton(getString(R.string.button_rate_rate), (dialog, which) -> {
                    if (ratingBar.getRating() > 3) {
                        Intent intent = new Intent(Intent.ACTION_VIEW);
                        intent.setData(Uri.parse("http://play.google.com/store/apps/details?id=" + getActivity().getPackageName()));
                        startActivity(intent);
                    } else {
                        new AlertDialog.Builder(getActivity())
                                .setTitle("Info")
                                .setMessage("Thank you for feedback")
                                .setPositiveButton("OK", (dialog1, which1) -> dialog1.dismiss())
                                .show();
                        dialog.cancel();
                    }
                })
                .setNeutralButton("Not Now", (dialog, which) -> dialog.cancel())
                .create();
        return alertDialog;
    }

    private void enableSendButton(boolean b) {
        alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setEnabled(b);
    }

    private void validateView() {
        float rating = ratingBar.getRating();
        if (rating < 4) {
            feedback.setVisibility(View.VISIBLE);
            alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setText(R.string.button_rate_send);
            if (feedback.getText().toString().isEmpty()) {
                enableSendButton(false);
            } else {
                enableSendButton(true);
            }
        } else {
            feedback.setVisibility(View.GONE);
            enableSendButton(true);
            alertDialog.getButton(DialogInterface.BUTTON_POSITIVE).setText(R.string.button_rate_rate);
        }
    }
}
