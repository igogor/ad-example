package com.test.apk.dto;

import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class Post implements Parcelable {
    @SerializedName("id")
    @Expose
    private int id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("siteLink")
    @Expose
    private String siteLink;
    @SerializedName("imgLink")
    @Expose
    private String imgLink;
    @SerializedName("shortDescr")
    @Expose
    private String shortDescr;
    @SerializedName("Descr")
    @Expose
    private String descr;
    @SerializedName("btnText")
    @Expose
    private String btnText;
    @SerializedName("openPage")
    @Expose
    private boolean openPage;
    @SerializedName("webView")
    @Expose
    private boolean webView;

    public Post() {}

    public Post(int id, String name, String siteLink, String imgLink, String shortDescr, String descr, String btnText, boolean openPage, boolean webView) {
        this.id = id;
        this.name = name;
        this.siteLink = siteLink;
        this.imgLink = imgLink;
        this.shortDescr = shortDescr;
        this.descr = descr;
        this.btnText = btnText;
        this.openPage = openPage;
        this.webView = webView;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSiteLink() {
        return siteLink;
    }

    public void setSiteLink(String siteLink) {
        this.siteLink = siteLink;
    }

    public String getImgLink() {
        return imgLink;
    }

    public void setImgLink(String imgLink) {
        this.imgLink = imgLink;
    }

    public String getShortDescr() {
        return shortDescr;
    }

    public void setShortDescr(String shortDescr) {
        this.shortDescr = shortDescr;
    }

    public String getDescr() {
        return descr;
    }

    public void setDescr(String descr) {
        this.descr = descr;
    }

    public String getBtnText() {
        return btnText;
    }

    public void setBtnText(String btnText) {
        this.btnText = btnText;
    }

    public boolean isOpenPage() {
        return openPage;
    }

    public void setOpenPage(boolean openPage) {
        this.openPage = openPage;
    }

    public boolean isWebView() {
        return webView;
    }

    public void setWebView(boolean webView) {
        this.webView = webView;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Post post = (Post) o;
        return id == post.id;
    }

    @Override
    public int hashCode() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            return Objects.hash(id);
        } else {
            int hash = 7;
            hash = 31 * hash + id;
            return hash;
        }
    }

    @Override
    public String toString() {
        return "Post{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", siteLink='" + siteLink + '\'' +
                ", imgLink='" + imgLink + '\'' +
                ", shortDescr='" + shortDescr + '\'' +
                ", descr='" + descr + '\'' +
                ", btnText='" + btnText + '\'' +
                ", openPage=" + openPage +
                ", webView=" + webView +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeString(this.siteLink);
        dest.writeString(this.imgLink);
        dest.writeString(this.shortDescr);
        dest.writeString(this.descr);
        dest.writeString(this.btnText);
        dest.writeByte(this.openPage ? (byte) 1 : (byte) 0);
        dest.writeByte(this.webView ? (byte) 1 : (byte) 0);
    }

    protected Post(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.siteLink = in.readString();
        this.imgLink = in.readString();
        this.shortDescr = in.readString();
        this.descr = in.readString();
        this.btnText = in.readString();
        this.openPage = in.readByte() != 0;
        this.webView = in.readByte() != 0;
    }

    public static final Creator<Post> CREATOR = new Creator<Post>() {
        @Override
        public Post createFromParcel(Parcel source) {
            return new Post(source);
        }

        @Override
        public Post[] newArray(int size) {
            return new Post[size];
        }
    };
}
