package com.test.apk.repository;

import com.test.apk.dto.Posts;
import com.test.apk.retrofit.PostService;
import com.test.apk.retrofit.ServiceBuilder;

import androidx.lifecycle.MutableLiveData;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class PostsRepository {
    private PostService service;
    private MutableLiveData<Posts> data = new MutableLiveData<>();
    public PostsRepository() {
        this.service = ServiceBuilder.buildService(PostService.class);
    }

    public MutableLiveData<Posts> getPosts() {
        service.getPosts().enqueue(new Callback<Posts>() {
            @Override
            public void onResponse(Call<Posts> call, Response<Posts> response) {
                data.setValue(response.body());
            }

            @Override
            public void onFailure(Call<Posts> call, Throwable t) {

            }
        });
        return data;
    }
}
