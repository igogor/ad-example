package com.test.apk;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import com.test.apk.dto.Post;
import com.test.apk.fragments.PostFragment;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class PostActivity extends SingleFragmentActivity {

    private final static String EXTRA = "post_";
    private final static String TITLE = "title_";

    @Override
    protected Fragment createFragment() {
        Post post = getIntent().getParcelableExtra(EXTRA);
        return PostFragment.newInstance(post);
    }

    public static Intent newIntent(Context context, Post post, String title) {
        Intent intent = new Intent(context, PostActivity.class);
        intent.putExtra(EXTRA, post);
        intent.putExtra(TITLE, title);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTitle(getIntent().getStringExtra(TITLE));
    }
}
