package com.test.apk;

import android.content.Intent;
import android.os.Bundle;

import com.test.apk.helpers.NetworkHelper;
import com.test.apk.viewmodel.PostsViewModel;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.ViewModelProviders;

public class SplashScreenActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        boolean isOnline = NetworkHelper.isOnline(this);
        if (isOnline) {
            ViewModelProviders.of(this).get(PostsViewModel.class).getPosts();
        }

        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
        startActivity(intent);
        finish();

    }

}
