package com.test.apk.retrofit;

import com.test.apk.dto.Posts;

import retrofit2.Call;
import retrofit2.http.GET;

public interface PostService {
    @GET("list")
    Call<Posts> getPosts();

}
