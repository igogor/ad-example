package com.test.apk.viewholder;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.test.apk.R;
import com.test.apk.dto.Post;
import com.test.apk.fragments.PostsFragment;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class PostViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.imageView)
    ImageView image;
    @BindView(R.id.post_title)
    TextView title;
    @BindView(R.id.textView)
    TextView text;
    @BindView(R.id.button)
    Button button;
    private PostsFragment.CallbackListener listener;
    private Post post;

    public PostViewHolder(@NonNull View itemView, PostsFragment.CallbackListener listener) {
        super(itemView);
        this.listener = listener;
        ButterKnife.bind(this, itemView);
        button.setOnClickListener(v -> listener.onButtonClick(post));
        itemView.setOnClickListener(viewListener);
    }

    public void bind(Post post) {
        this.post = post;
        if (post.getImgLink() == null) {
            image.setImageResource(R.drawable.no_image_icon);
        } else {
            Picasso.get().load(post.getImgLink()).into(image);
        }
        title.setText(post.getName());
        text.setText(post.getShortDescr());
        button.setText(post.getBtnText());
    }

    private View.OnClickListener viewListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            listener.onPostClick(post);
        }
    };

}
