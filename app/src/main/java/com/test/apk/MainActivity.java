package com.test.apk;

import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;

import com.google.ads.consent.ConsentForm;
import com.google.ads.consent.ConsentFormListener;
import com.google.ads.consent.ConsentInfoUpdateListener;
import com.google.ads.consent.ConsentInformation;
import com.google.ads.consent.ConsentStatus;
import com.google.ads.mediation.admob.AdMobAdapter;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.doubleclick.PublisherAdRequest;
import com.google.android.gms.ads.doubleclick.PublisherInterstitialAd;
import com.test.apk.dto.Post;
import com.test.apk.fragments.PostsFragment;
import com.test.apk.fragments.RateDialogFragment;
import com.test.apk.helpers.NetworkHelper;

import java.net.MalformedURLException;
import java.net.URL;

import androidx.fragment.app.Fragment;

public class MainActivity extends SingleFragmentActivity implements PostsFragment.CallbackListener {
    private PublisherInterstitialAd interstitialAd;
    private Post currentPost;
    private ConsentForm form;
    private ConsentStatus currentConsent;
    private SharedPreferences preferences;

    @Override
    protected Fragment createFragment() {
        return new PostsFragment();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        preferences = getPreferences(MODE_PRIVATE);
        int totalCount = preferences.getInt("numRun", 0);
        if (totalCount <= 2) {
            totalCount++;
            preferences.edit().putInt("numRun", totalCount).apply();
            if (totalCount == 2) {
                showRateDialog();
            }
        }

        requestConsent();
    }

    private void showRateDialog() {
        RateDialogFragment fr = new RateDialogFragment();
        fr.show(getSupportFragmentManager(), "FR");
    }

    private void requestConsent() {
        ConsentInformation consentInformation = ConsentInformation.getInstance(this);
        String[] publisherId = {""};
        consentInformation.requestConsentInfoUpdate(publisherId, new ConsentInfoUpdateListener() {
            @Override
            public void onConsentInfoUpdated(ConsentStatus consentStatus) {
                currentConsent = consentStatus;
                boolean inEU = consentInformation.isRequestLocationInEeaOrUnknown();
                if (inEU) {
                    if (consentStatus == ConsentStatus.PERSONALIZED ||
                            consentStatus == ConsentStatus.NON_PERSONALIZED) {
                        loadAd();
                    } else
                        loadConsentForm();
                } else
                    loadAd();
            }

            @Override
            public void onFailedToUpdateConsentInfo(String reason) {

            }
        });
    }

    private void loadAd() {
        PublisherAdRequest.Builder requestBuilder = new PublisherAdRequest.Builder();
        if (currentConsent == ConsentStatus.NON_PERSONALIZED) {
            Bundle extras = new Bundle();
            extras.putString("npa", "1");
            requestBuilder.addNetworkExtrasBundle(AdMobAdapter.class, extras);
        }
        if (interstitialAd == null) {
            interstitialAd = new PublisherInterstitialAd(MainActivity.this);
            interstitialAd.setAdUnitId("/6499/example/interstitial");
            interstitialAd.setAdListener(new AdListener() {
                @Override
                public void onAdClosed() {
                    super.onAdClosed();
                    if (currentPost != null)
                        startActivity(PostActivity.newIntent(MainActivity.this, currentPost, getTitle().toString()));
                }

                @Override
                public void onAdLoaded() {
                    super.onAdLoaded();
                    interstitialAd.show();
                }
            });
        }
        interstitialAd.loadAd(requestBuilder.build());
    }

    private void loadConsentForm() {
        URL privacyUrl = null;
        try {
            privacyUrl = new URL("http://timy.ua/privacyurl");
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        form = new ConsentForm.Builder(this, privacyUrl)
                .withListener(new ConsentFormListener() {
                    @Override
                    public void onConsentFormLoaded() {
                        form.show();
                    }

                    @Override
                    public void onConsentFormClosed(ConsentStatus consentStatus, Boolean userPrefersAdFree) {
                        currentConsent = consentStatus;
                        loadAd();
                    }
                })
                .withPersonalizedAdsOption()
                .withNonPersonalizedAdsOption()
                .build();
        form.load();
    }

    @Override
    public void onPostClick(Post post) {
        currentPost = post;
        if (post.isOpenPage()) {
            if (NetworkHelper.isOnline(this)) {
                loadAd();
            } else {
                startActivity(PostActivity.newIntent(MainActivity.this, currentPost, getTitle().toString()));
            }
        } else {
            openWebView(post.getSiteLink());
        }
    }

    @Override
    public void onButtonClick(Post post) {
        if (post.isWebView()) {
            openWebView(post.getSiteLink());
        } else {
            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(post.getSiteLink())));
        }
    }

    private void openWebView(String link) {
        startActivity(WebViewActivity.newIntent(this, link, getTitle().toString()));
    }
}
